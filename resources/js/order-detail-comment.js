;(function($, window) {
    "use struct";

    $(".commit-comment").on("tap", function() {
        var comm = {};
        var input = $("textarea");
        for (var i = 0; i < input.length; i++) {
            if (input.eq(i).val().trim().length < 5) {
                input.eq(i).parent().parent().find(".tips").css("display", "block");
                return;
            }
            input.eq(i).parent().parent().find(".tips").css("display", "none");
            comm[i] = input.eq(i).val().trim();
        }
        // console.log(comm);
        // 提交评论
        $.ajax({
            url: '',
            type: 'post',
            dataType: 'json',
            data: comm,
        })
        .done(function() {
            console.log("success");
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });

    });
})(Zepto, window);