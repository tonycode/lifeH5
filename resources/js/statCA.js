var searchCA = "v160830_hysc-common_h5_search_r1$search.";

function searchMdse() {
	var keyword = $("#keyWord").val();
	if (!keyword) {
		keyword = $("#keyWord").attr("placeholder");
	}
	if (keyword) {
		keyword = encodeURIComponent(keyword);
	}
	requestCA(searchCA + keyword);
	var url = "/serchMdsePageList_v2/serchMdseList_index.htm?searchWord="+keyword;
	//if (window.location.href.indexOf("serchMdseList_index.htm") != -1) {
		window.location.href=url;
	//} else {
	//	window.open(url);
	//}
	
}

function scanCA(obj) {
	bindCA($(obj).find("[ca]"));
}

function bindCA(obj, ca){
	$(obj).each(function(){
		$(this).click(function(){
			var c=$(this).attr("ca");
			if (typeof(ca) != 'undefined') {
				c = ca;
			}
			requestCA(c);
		});
	});
}

function requestCA(ca) {
	var img = new Image(1,1);
	img.src=('https:'==document.location.protocol?'https':'http')+
				'://data.chexiang.com/ca.gif?'+
				'ca='+ca+
				'&_v='+new Date();
}

$(function(){
	var keywords = $("#keyWord");
	if (keywords && keywords.length && !keywords.val()) {
		$.get("/yxsc/yt/ssgjz/index.shtml", function(data){
			keywords.attr("placeholder", data);
		});
	}
});