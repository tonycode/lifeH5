(function($) {
    var showpopin = function(){
        $(".mask").show();
        $("#mypop").show();        
    }
    var hidepopin = function(){
        $(".mask").hide();
        $("#mypop").hide();        
    }
    $(".button1").on("click", function () {
       showpopin()
    })
    $(".pop-close").on("click", function () {
       hidepopin()
    })
    $("#btn_cancle").on("click", function () {
       hidepopin()
    })
    //usercard validate foo
    var mobileValidate = function(){
        var $input = $("#faq_user"),                
            $codebtn = $("#faq_getcode"),            
            $submitbtn = $("#faq_submit"),
            $tip = $input.siblings(".tiparea").find('.tip'),  
            $wrongMsg = $("#faqUserErr"), 
            $wrongTxt = $wrongMsg.find(".errTxt"),          
            userinput = $input.val();               
            state = false;              
            $input.on("blur", validate);
            $codebtn.on("click",validate);
            $submitbtn.on("click",validate); 
            function validate() {
                $tip.hide();
                var userinput2 = $input.val();
                userinput = userinput2.replace(/\s+/g, "");
                check_phone(userinput);
            };
            function check_phone(a) {
                if (a == "") {
                    $wrongTxt.text("手机号码不能为空");
                    $wrongMsg.show();
                    $input.addClass("input-red");
                    state = false;   
                    return false
                }
                var b = /^1\d{10}$/;
                if (!b.test(a)) {
                    $wrongTxt.text("格式错误，请输入正确的手机号码");
                    $wrongMsg.show();
                    $input.addClass("input-red");
                    state = false;   
                    return false
                }
                $wrongTxt.text("");
                $wrongMsg.hide();
                state = true;  
                return true
            };
            $input.on("focus", function() {
                $wrongMsg.hide().siblings(".m-err").hide(); 
                $input.siblings('#codeMsg').hide();
                $input.removeClass("input-red");
            });
            return {
                $input: $input,
                validate: validate,
                userinput: userinput
            };
            
    };    
    var countDownTemp = "<span class='timeLeft'>120</span>秒后重发";
    // 手机 --> 手机验证码
    var $mobileCapcha = $(".sms");

    mobileCapcha = function() {
        var dom = $mobileCapcha,
            btn = dom.find(".get-btn"),
            $tip = dom.find(".m-notice.tip"),
            countingDown = false,
            isEnabled = true;
        var $input = $("#faq_captcha"),
            $wrongMsg = $("#mobileErr"), 
            $wrongTxt = $wrongMsg.find(".errTxt");
        btn.on("click", function() {  
            if(!state) return;        
            if (countingDown) return;
            $tip.hide();                
                            var data =0;
                            if (data === 0) {
                                // 倒计时
                                btn.html(countDownTemp);
                                disable();                               
                                $wrongMsg.hide();
                                $input.removeClass("input-red");
                                $("#mobileErr").hide();

                                // 修改样式
                                countDown(btn.find(".timeLeft"), 120 * 1000, function() {
                                    btn.html("获取验证码").removeClass("btn-gray btn-disable");
                                    enable();
                                    countingDown = false;

                                    $tip.hide();
                                });
                                countingDown = true;
                            } else if (data === -1) {

                                $tip.hide();
                                $wrongTxt.html("<i></i>120秒内不可重复获取验证码");
                                $wrongMsg.show();
                                $input.addClass("input-red");
                                state = false;

                            } else if (data === -2) {

                                $tip.hide();
                                $wrongTxt.html("<i></i>24小时内获取手机验证码达到最大次数");                                    
                                $wrongMsg.show();
                                $input.addClass("input-red");
                                state = false;
                            } 
            // $.when(account.validate()).done(function() {
            //     if (account.state() && enabled()) {
            //         $.ajax({
            //             url: base + '/account/toGetPhoneCodeChk.htm' + "?d=" + new Date().getMilliseconds(),
            //             type: "POST",
            //             asyn: false,
            //             data: {
            //                 Num: account.$input.val(),
            //                 'temStyle': $("#mobTemStyle").val(),
            //                 'typeId': $("#mobileTypeId").val(),
            //                 'validCode': $("#validCode").val()
            //             },
            //             dataType: "JSON",
            //             success: function(data) {
            //                 $("#chkclkNum").val("1");
            //                 if (data === 0) {
            //                     // 倒计时
            //                     btn.html(countDownTemp);
            //                     disable();
            //                     $tip.show();

            //                     $wrongMsg.hide();
            //                     $("#mobileErr").hide();

            //                     // 修改样式
            //                     countDown(btn.find(".timeLeft"), 120 * 1000, function() {
            //                         btn.html("获取验证码").removeClass("btn-gray btn-disable");
            //                         enable();
            //                         countingDown = false;
            //                         $tip.hide();
            //                     });
            //                     countingDown = true;
            //                 } else if (data === -1) {

            //                     $tip.hide();
            //                     $wrongTxt.html("<i></i>120秒内不可重复获取验证码");
            //                     $wrongMsg.show();
            //                     $input.addClass("input-red");
            //                     state = false;

            //                 } else if (data === -2) {

            //                     $tip.hide();
            //                     $wrongTxt.html("<i></i>24小时内获取手机验证码达到最大次数");
            //                     $("#mobileErr").show();
            //                     // $wrongMsg.show();
            //                     $input.addClass("input-red");
            //                     state = false;
            //                 } else if (data === 3) {

            //                     $tip.hide();
            //                     $wrongMsg = $("#mobileErr1"); //防止后台效验同时出现,lwf0617
            //                     //                                   $wrongTxt = $wrongMsg.find(".errTxt"),
            //                     $("#errTxtValidCode").html("<i></i>哎呀，图片验证码好像不正确");
            //                     $wrongMsg.show();
            //                     $input.addClass("input-red");
            //                     state = false;
            //                 }
            //             },
            //             error: function() {
            //                 capcha.showWrongMsg("请求发送验证码失败，请重新再试！");
            //             }
            //         });
            //     }
            // })
        });

        function disable() {
            $('#faq_user').attr("readonly",true);
            btn.addClass("btn-gray btn-disable");
            isEnabled = false;
        }

        function enable() {
            
            $('#faq_user').removeAttr("readonly");
            if (countingDown) return;
            
            btn.removeClass("btn-gray btn-disable");
            isEnabled = true;
        }

        function enabled() {
            return isEnabled;
        }

        function countDown($timeLeft, time, finish) {
            var total = time,
                left = time;

            var id = setInterval(function() {
                left -= 1000;
                $timeLeft.text(parseInt(left / 1000));
            }, 1000);
            setTimeout(function() {
                clearInterval(id);
                finish();
            }, total);
        }

        return {
            dom: dom,
            enable: enable,
            disable: disable
        };
    }();
    // 验证码检测 -- 只判断非空
        var state = true;
        var capcha = function() {
            var $input = $("#faq_captcha"),
                $submitbtn = $("#faq_submit"),
                $tip = $mobileCapcha.find(".tiparea").find('.tip'),
                $wrongMsg = $("#mobileErr"), 
                $wrongTxt = $wrongMsg.find(".errTxt"),
                state = false;

            function getState() {
                return state;
            }
            $input.on("blur", validate);
            $submitbtn.on("click",validate); 
            function validate() {
                $tip.hide();
                if ($.trim($input.val()).length === 0) {
                    $wrongTxt.text("验证码不正确");
                    $wrongMsg.show();
                    $input.addClass("input-red");
                    state = false;
                } else {
                    state = true;
                }
            }

            $input.on("focus", function() {
                //$tip.show();
                $wrongMsg.hide().siblings(".m-err").hide(); 
                $input.siblings('#codeMsg').hide();
                $input.removeClass("input-red");
            });

            return {
                $input: $input,
                state: getState,
                validate: validate,
                showWrongMsg: function(msg) {
                    $wrongTxt.text(msg);
                    $wrongMsg.show();
                    $input.addClass("input-red");
                }
            };
        }();
})(Zepto);