;
(function($, ECar) {
    "use strict";
    //ios btn的active属性生效
    var a = document.getElementsByTagName('a');
    for (var i = 0; i < a.length; i++) {
        a[i].addEventListener('touchstart', function() {}, false)
    }

    // swiper初始化
    var swiper = new Swiper('#slides', {
        pagination: '#slides .swiper-pagination'
    });

    //展开收回
    $(".float_dot").click(function() {
            var _height = $(this).parents('.h_control').height();
            $(this).data("height") ? "" : $(this).data("height", _height);

            if ($(this).hasClass('up')) {
                $(this).removeClass('up').addClass('down');
                $(this).parents('.h_control').css("height", "1.12rem");
            } else {
                $(this).removeClass('down').addClass('up');
                $(this).parents('.h_control').css("height", $(this).data("height") + "px");
            }
        })
        //切换
    var tabsSwiper = new Swiper('#swiperContainer', {
        speed: 500,
        onSlideChangeStart: function() {
            $(".nav-tabs .active").removeClass('active');
            $(".nav-tabs li").eq(tabsSwiper.activeIndex).addClass('active')
        }
    });


    // 选择地区
    $("#findCity").click(function() {
        var that = $("#selectCity");
        ECar.mSelect.show({
            provinceUrl: "/resources/data/province.json",
            cityUrl: "/resources/data/city.json",
            data: that.data("address"),
            headerMsg: "可配送区域",
            closeFn: function() {
                var address = ECar.mSelect.result;
                if (address.code === "") return;
                that.focus().val(address.province.name + address.city.name + address.region.name)
                    .data("address", JSON.stringify(address)).blur();
            }
        });
    });

    $(".nav-tabs li").on('touchstart mousedown', function(e) {
        e.preventDefault();
        $(".nav-tabs .active").removeClass('active');
        $(this).addClass('active');
        tabsSwiper.slideTo($(this).index());
    });
    $(".nav-tabs li").click(function(e) {
        e.preventDefault()
    });

    //menu切换
    $(".menu .tag").on("tap", function() {

        //图片重设 变蓝
        $(this).addClass("sel").siblings().removeClass("sel");

        //点击分类
        if ($(this).hasClass("js_category")) {
            if ($("body").hasClass("cannot-scroll")) {
                $(".js_index").trigger("tap");
            } else {
                showCategory();
            }
        } else {
            hideCategory();
        }
    });

    $(".masking").on("tap", function() {
        $(".js_index").trigger("tap");
        menuShowAndHide();
    });

    //弹出分类
    function showCategory() {
        $(".category").css("display", "block");
        $("html, body").addClass("cannot-scroll");
    }
    //隐藏分类
    function hideCategory() {
        $(".category").css("display", "none");
        $("html,body").removeClass("cannot-scroll");
    }

    //头部总菜单控制
    function menuShowAndHide(){
        var menuWapper = $(".menu-wapper"),
            menuShowType = menuWapper.attr("showType"),
            flagTime;
        clearTimeout(flagTime);
        flagTime = setTimeout(function(){
            if(menuShowType == "false"){
                menuWapper.removeClass("slideOutRight").addClass("slideInRight").attr("showType","true");
            }else{
                menuWapper.removeClass("slideInRight").addClass("slideOutRight").attr("showType","false");
                hideCategory();
            }
        },300);
    }

    $(".js_menu_btn").on("tap", function(){
        menuShowAndHide();
    });
})(Zepto, window.ECar || (window.ECar = {}));