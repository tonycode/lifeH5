;(function($, ECar) {
	"use strict";
//ios btn的active属性生效
	var a=document.getElementsByTagName('a');for(var i=0;i<a.length;i++){a[i].addEventListener('touchstart',function(){},false)}
//图片滑动
	window.onload = function(){
		var num = $(".scrollView").children().length;
		var width = 1.84*num;
		$(".scrollView").css("width",width+"rem");
	};
//是否需要发票
	$(".bill_sec").click(function(){
		$(".icon-select").removeClass('active');
		$(this).children('.icon-select').addClass('active');
		if ($(this).hasClass('need')) {
			$(".needInfo").css("display","block");
		}else{
			$(".needInfo").css("display","none");
		}
	});
//发票抬头
	$(".sec_b").click(function(){
		$(".sec_b").removeClass('focus');
		$(this).addClass('focus');
		if ($(this).hasClass('company')) {
			$(".needInput input").css("display","block");
		}else{
			$(".needInput input").css("display","none");
		}
	});

//仿IOS的select
	var xs = new xScrollSelect({
		spaceNum: 2,
		itemWidth: 80,
		showCover: true,
		items: [
			{name: '明细'},
			{name: '汽车用品'},
			{name: '日用品'},
			{name: '办公用品'},
			{name: '办公用品'}
		],         
		onConfirm: function(params) {
			var o = JSON.stringify(params.data.name);
			o=o.replace(/\"/g,"");
			document.getElementById('billInfo').innerHTML = o;
		}
	});
	document.getElementById("billInfo").addEventListener('click', function() {
		xs.show();
	}, false);

})(Zepto, window.ECar || (window.ECar = {}));