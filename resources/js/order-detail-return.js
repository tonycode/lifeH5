;(function($, window) {
    "use struct";

    // 提交btn
    $(".commit-return").on("tap", function() {
        var comm = {};
        var input = $(".good_number");
        for (var i = 0; i < input.length; i++) {
            comm[i] = input.eq(i).val();
        }
        var reason = $(".select-reason")[0].value;
        comm["reason"] = reason;
        if (reason == "other") {
            var detail = $(".return-detail textarea").val().trim();
            comm["detail"] = detail;
        }


        console.log(comm);
        // 提交

    });




    // 状态控制
    function isDisabled(numDom) {
        val = numDom.val();
        max = numDom.parents(".return-info").children(".buy-num").children("em").html();
        reduceDom = numDom.siblings(".btn_reduce");
        addDom = numDom.siblings(".btn_add");
        if (val <= 0)
            reduceDom.addClass("disabled");
        else
            reduceDom.removeClass("disabled");
        if (val - max >= 0)
            addDom.addClass("disabled");
        else
            addDom.removeClass("disabled");
    }
    // 退货数量 "-"
    $(".btn_reduce").on("tap", function() {
        var max = $(this).parents(".return-info").children(".buy-num").children("em").html();
        var numDom = $(this).siblings(".good_number");
        var val = numDom.val();
        numDom.val(val > 0 ? --val : 0);
        isDisabled(numDom);
    });
    // 退货数量 "+"
    $(".btn_add").on("tap", function() {
        var max = $(this).parents(".return-info").children(".buy-num").children("em").html();
        var numDom = $(this).siblings(".good_number");
        var val = numDom.val();
        numDom.val((val - max < 0) ? ++val : max);
        isDisabled(numDom);
    })
        // 退货数量 "自定义"
    $(".good_number").blur(function() {
        var max = $(this).parents(".return-info").children(".buy-num").children("em").html();
        var val = $(this).val();
        if (val >= 0 && val - max <= 0) {
            $(this).val(parseInt(0 + val));
            isDisabled($(this));
            return;
        }
        $(this).val(0);
    });


    // 查看更多商品 "展开"
    $(".show-more").on("tap", function() {
        $(this).prev(".comb-goods").children("li").css("display", "block");
        $(this).next(".show-less").show();
        $(this).hide();
    });
    // 查看更多商品 "收缩"
    $(".show-less").on("tap", function() {
        $(this).prev().prev(".comb-goods").children("li").css("display", "");
        $(this).prev().show();
        $(this).hide();
    });

    // select退货原因
    $(".select-reason").on("change", function() {
        if (this.value == "other")
            $(".return-detail").css("visibility", "");
        else
            $(".return-detail").css("visibility", "hidden");
    })

})(Zepto, window);