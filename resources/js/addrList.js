;
(function($, window) {
	"use strict";

	$(function() {
		$(".touch").swipeLeft(function() {
			var self = $(this),
				oDelete = self.find(".delete");

			$(".touch").removeClass("selected");
			self.addClass("selected");
			oDelete.css("line-height", self.height() + "px");
		}).tap(function(e) {
			var target = e.target,
				self = $(this),
				sNodeName = target.nodeName.toLowerCase();

			//删除地址
			if (sNodeName === "a" && $(target).hasClass("delete")) {
				//后台删除操作

				//删除成功后操作
				$(target).parent("li").css("border", "0").animate({
					height: 0,
					opacity: 0
				}, 300, function() {
					$(this).remove();
				});
				return false;
			}

			//编辑地址
			if (sNodeName === "i" && $(target).hasClass("icon_update")) {
				setTimeout(function() {
					//编辑地址操作

				}, 320);
				return;
			}

			//隐藏删除按钮
			$(".touch").removeClass("selected");

			//切换选中状态
			if (!self.hasClass("current")) {
				$(".touch").removeClass("current");
				self.addClass("current");
			}

		}).swipeRight(function() {
			$(this).removeClass("selected");
		});

		$(".address_btn").tap(function() {
			//新增地址操作

		});
	});
})(Zepto, window);