;
(function($) {
	"use strict";
	    var order_view = $(".order-view"),
	        check_con = $(".check-con");

	    order_view.on("click", function() {  
			ECar.mModal.warn({
				template: check_con.show(),
				content: $(".check-con")[0].outerHTML,
				title: "查看退货",
				hasTitle: false,
				width: 750,
				height: 0,
				loadFn: function() {
					$(".close").on("click", function(){
			          ECar.mModal.hide();
			          check_con.hide();
				   });
				},
				closeFn: function() {
					check_con.hide();
				} 
			});
	    });

	    // 查看更多商品 "展开"
        $(".js_down").on("tap", function() {
            $(this).parent().children(".comb").children("li").css("display", "block");
            $(this).next(".js_up").show();
            $(this).hide();
        });
        // 查看更多商品 "收缩"
        $(".js_up").on("tap", function() {
            $(this).parent().children(".comb").children("li").css("display", "");
            $(this).prev().show();
            $(this).hide();
        });

})(Zepto, window.ECar || (window.ECar = {}));	
