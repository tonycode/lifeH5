;
(function($, window) {
	"use strict";

	$(function() {
		// 切换设置默认地址按钮
		$('.check_btn').tap(function() {
			$(this).toggleClass("checked");
		});

		// 选择地区
		$(".icon_address, .order_address").tap(function() {
			var that = $(".order_address");
			that.focus();

			ECar.mSelect.show({
				provinceUrl: "/resources/data/province.json",
				cityUrl: "/resources/data/city.json",
				regionUrl: "/resources/data/region.json",
				data: that.data("address"),
				closeFn: function() {
					var address = ECar.mSelect.result;
					if (address.code === "") return;

					that.focus().val(address.province.name + address.city.name + address.region.name)
						.data("address", JSON.stringify(address)).blur();
				}
			});
		});

		//  输入区域检测
		$(".ipt").on("blur", function() {
			var result = checkAddr(),
				$submit = $(".submit"),
				$error = $(".error_content");

			if (result.code === 1) {
				$error.hide().text("");
				$submit.removeClass("disabled");
			} else {
				$error.text(result.msg).show();
				$submit.addClass("disabled");
			}
		});

		// 检验地址
		function checkAddr() {
			var $ipt = $(".ipt"),
				that = null,
				type = "",
				msg = "",
				result = {};

			for (var i = 0, len = $ipt.length; i < len; i++) {
				that = $ipt.eq(i);
				type = that.data("type");

				switch (type) {
					case "name":
						that.val().length < 2 ? (msg = "收货人姓名至少填写两个字符") : "";
						break;
					case "phone":
						that.val().length < 1 ? (msg = "请输入手机号码") : "";
						break;
					case "address":
						that.val().length < 1 ? (msg = "请选择所在地区") : "";
						break;
					case "area":
						that.val().length < 1 ? (msg = "请输入详细地址") : "";
						break;
				}

				if (msg.length) {
					result.code = 0;
					result.msg = msg;
					break;
				} else {
					result.code = 1;
					result.msg = "";
				}
			}

			return result;
		}
	});
})(Zepto, window);