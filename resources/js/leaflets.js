;(function($, ECar) {
	"use strict";
//touch事件
	var jiaSrc = $("#chexiangjia").attr('src');
	var huiSrc = $("#chexianghui").attr('src');
	var chexiang = $(".chexiang");
	chexiang.on('touchstart',touch,false);
	chexiang.on('touchend',touch,false);
	function touch(event){
    	var src = $(this).attr('src');
    	switch(event.type){
    		case 'touchstart' : 
    			$("#chexianghui,#chexiangjia").attr('src', src);
    			break;
    		case 'touchend' : 
    			if ($(this).hasClass('chexiangjia')) {
    				$("#chexianghui").attr('src', huiSrc);
    			}else{
    				$("#chexiangjia").attr('src', jiaSrc);
    			}
    	}
		
	}
})(Zepto, window.ECar || (window.ECar = {}));