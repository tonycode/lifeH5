;(function($, ECar) {
    "use strict";
//ios btn的active属性生效
    var a=document.getElementsByTagName('a');for(var i=0;i<a.length;i++){a[i].addEventListener('touchstart',function(){},false)}

// swiper初始化
    var mySwiper = new Swiper('.swiper-container', {
        loop: true,
        autoplay: 0,
        autoplayDisableOnInteraction: false,
        pagination : '.pagination'
    });

//返回顶部
    $(window).on("scroll",function(){
        if( $(window).scrollTop() > 200 ) {
            $("#gotopbtn").show();
        }else {
            $("#gotopbtn").hide();
        }
    });
    (function(){
        var btn = document.getElementById("gotopbtn");
        var timer = null;
        var isTop = true;

        //获取页面可视区高度
        var clientHeight = document.documentElement.clientHeight;

        //滚动条滚动时触发
        btn.onclick = function() {

        //设置定时器
        timer = setInterval(function() {
                //获取滚动条距离顶部高度
                var osTop = document.documentElement.scrollTop || document.body.scrollTop;
                var ispeed = Math.floor(-osTop / 7);
                document.documentElement.scrollTop = document.body.scrollTop = osTop+ispeed;

                //到达顶部，清除定时器
                if (osTop == 0) {
                    clearInterval(timer);
                };
                isTop = true;
            }, 30);
        };
    })();

    //menu切换
    $(".menu .tag").on("tap", function() {
        //图片reset 变灰
        var sel = $(".menu > .sel").children("img");
        for (var i = 0; i < sel.length; i++) {
            var src = sel.eq(i).attr("src").replace("_sel", "");
            sel.eq(i).attr("src", src);
        }
        $(".menu > .sel").removeClass("sel");
        //图片重设 变蓝
        $(this).addClass("sel");
        sel = $(".menu > .sel").children("img");
        var src = sel.attr("src").replace(".png", "_sel.png");
        sel.attr("src", src);

        //点击分类
        if ($(this).hasClass("js_category")) {
            if ($("body").hasClass("cannot-scroll")) {
                $(".js_index").trigger("tap");
            }
            else {
                showCategory();
            }
        }
        else {
            hideCategory();
        }
    });

    $(".masking").on("tap", function() {
        $(".js_index").trigger("tap");
    });

    //弹出分类
    function showCategory() {
        $(".category").css("display", "block");
        $("html").addClass("cannot-scroll");
        $("body").addClass("cannot-scroll");
    }
    //隐藏分类
    function hideCategory() {
        $(".category").css("display", "none");
        $("html").removeClass("cannot-scroll");
        $("body").removeClass("cannot-scroll");
    }

$(".search-icon").on("tap", function() {
        searchMdse();
    });

    // 广播滚动
    (function() {
        var adShow = $(".ad-show");
        var num = adShow.children().length;
        adShow.append(adShow.children().eq(0).clone());

        // 每条广播停留
        var delay = 1000;
        // 动画速度
        var speed = 1000;
        // 动画
        oneLoop();
        window.setInterval(function() {
            oneLoop();
        }, num*(delay + speed) + 200);

        function oneLoop() {
            adShow.css("margin-top", 0);
            var i = 0;
            window.setTimeout(function() {
                if (i == 0) {
                    // console.log("animate():" + (i+1));
                    adShow.animate({"margin-top": -0.5*(i+1) + "rem"}, speed);
                    i++;
                }
                var loop = window.setInterval(function() {
                    if (i >= num-1) {
                        window.clearInterval(loop);
                    }
                    // console.log("animate():" + (i+1));
                    adShow.animate({"margin-top": -0.5*(i+1) + "rem"}, speed);
                    i++;
                }, delay + speed);
            }, delay);
        }
    })();



})(Zepto, window.ECar || (window.ECar = {}));