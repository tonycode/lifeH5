;
(function($, window) {
	"use strict";

	// 搜索
	$(".search_btn").tap(function() {
		var condition = $.trim($(".search").val()); // 查询条件

		if (condition.length === 0) {
			$(".search").val("");
			return;
		}

		search(condition, "all", true);
	});

	// 导航
	$(".good_nav li").tap(function() {
		var self = $(this),
			condition = $.trim($(".search").val()), // 查询条件
			type = self.data("type"),
			bUp = true; // 升序

		if (condition.length === 0) {
			$(".search").val("");
			return;
		}

		// 切换升序降序
		if (self.hasClass("current")) {
			self.toggleClass("down");
		}

		// 切换导航状态
		self.addClass("current").siblings().removeClass("current down");

		bUp = !self.hasClass("down");
		search(condition, type, bUp);

	})

	function search(str, type, isUp) {
		var bUp = isUp || true; // 是否升序

		// 后台查询操作
	}
	//返回顶部
    $(window).on("scroll",function(){
        if( $(window).scrollTop() > 200 ) {
            $("#gotopbtn").show();
        }else {
            $("#gotopbtn").hide();
        }
    });
    (function(){
        var btn = document.getElementById("gotopbtn");
        var timer = null;
        var isTop = true;

        //获取页面可视区高度
        var clientHeight = document.documentElement.clientHeight;

        //滚动条滚动时触发
        btn.onclick = function() {

        //设置定时器
        timer = setInterval(function() {
                //获取滚动条距离顶部高度
                var osTop = document.documentElement.scrollTop || document.body.scrollTop;
                var ispeed = Math.floor(-osTop / 7);
                document.documentElement.scrollTop = document.body.scrollTop = osTop+ispeed;

                //到达顶部，清除定时器
                if (osTop == 0) {
                    clearInterval(timer);
                };
                isTop = true;
            }, 30);
        };
    })();
})(Zepto, window);